package com.verboven.springbootjpamulti.controller;

import com.verboven.springbootjpamulti.entity.Event;
import com.verboven.springbootjpamulti.entity.Foodtruck;
import com.verboven.springbootjpamulti.exception.ResourceNotFoundException;
import com.verboven.springbootjpamulti.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class EventController {

    @Autowired
    private EventRepository eventRepository;

    // Get all events
    @GetMapping("/events")
    public Collection<Event> getAllEvents(){
        return eventRepository.findAll();
    }

    // Get a single event by id
    @GetMapping("/events/{id}")
    public Event getEventbyId(@PathVariable Long id){
        return eventRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Event", "id", id));
    }

    // Get a single event's foodtrucks by id
    @GetMapping("/events/{id}/foodtrucks")
    public Collection<Foodtruck> getEventFoodtrucks(@PathVariable Long id) {
        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Event", "id", id));

        return event.getFoodtrucks();
    }

    // Create a new event
    @PostMapping("/events")
    public Event createEvent(@Valid @RequestBody Event event){
        return eventRepository.save(event);
    }

    // Delete an event
    @DeleteMapping("/events")
    public ResponseEntity<?> deleteEvent(@PathVariable Long id) {
        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Event", "id", id));

        eventRepository.delete(event);

        return ResponseEntity.ok().build();
    }

    // Update an event
    @PutMapping("/events/{id}")
    public Event updateEvent(@Valid @RequestBody Event eventDetails, @PathVariable Long id) {

        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Event", "id", id));

        event.setLocation(eventDetails.getLocation());
        event.setName(eventDetails.getName());
        event.setDate(eventDetails.getDate());
        event.setFoodtrucks(eventDetails.getFoodtrucks());

        return eventRepository.save(event);
    }

}
