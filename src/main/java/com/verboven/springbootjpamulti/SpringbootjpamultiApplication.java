package com.verboven.springbootjpamulti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootjpamultiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootjpamultiApplication.class, args);
    }

}

