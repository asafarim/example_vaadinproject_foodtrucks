package com.verboven.springbootjpamulti.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="foodtrucks")
public class Foodtruck {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "foodtruck_id")
    private Long id;

    @NotNull
    @Size(max = 100)
    private String name;

    @NotNull
    private int staffnumber;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonBackReference
    @JoinTable(name = "foodtrucks_events",
            joinColumns = @JoinColumn(name = "foodtruck_id", referencedColumnName = "foodtruck_id"),
            inverseJoinColumns = @JoinColumn(name = "event_id", referencedColumnName = "event_id"))
    private Set<Event> events = new HashSet<Event>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "foodtruck", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Product> products = new HashSet<>();

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStaffnumber() {
        return staffnumber;
    }

    public void setStaffnumber(int staffnumber) {
        this.staffnumber = staffnumber;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addProduct(Product product){
        product.setFoodtruck(this);
        products.add(product);
    }

    public void removeProduct(Product product){
        product.setFoodtruck(null);
        products.remove(product);
    }

    public void addEvent(Event event){
        event.getFoodtrucks().add(this);
        events.add(event);
    }

    public void removeEvent(Event event){
        events.remove(event);
        event.getFoodtrucks().remove(this);
    }
}
