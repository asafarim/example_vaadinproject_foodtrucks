package com.verboven.springbootjpamulti.entity;

public enum ProductType {

    Drink, Food, Veggie

}
