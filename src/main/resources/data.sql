Insert into foodtrucks (FOODTRUCK_ID, NAME, STAFFNUMBER) values(100001, 'Hotdogs Jef', 1);
Insert into foodtrucks (FOODTRUCK_ID, NAME, STAFFNUMBER) values(200001, 'Veggies', 2);

Insert into products (PRODUCT_ID, NAME, TYPE, FOODTRUCK_ID) values(100001, 'Jefdog', 'Food', 100001);
Insert into products (PRODUCT_ID, NAME, TYPE, FOODTRUCK_ID) values(200001, 'Veggiebowl', 'Veggie', 200001);
Insert into products (PRODUCT_ID, NAME, TYPE, FOODTRUCK_ID) values(300001, 'Cola', 'Drink', 100001);

Insert into events (EVENT_ID, EVENT_DATE, LOCATION, NAME) values (100001, '2016-05-08', 'Stadion', 'KV vs FC');
Insert into events (EVENT_ID, EVENT_DATE, LOCATION, NAME) values (200001, '2016-06-08', 'Markt', 'Zomerpret');

Insert into FOODTRUCKS_EVENTS (FOODTRUCK_ID, EVENT_ID) values (100001, 100001);
Insert into FOODTRUCKS_EVENTS (FOODTRUCK_ID, EVENT_ID) values (100001, 200001);
Insert into FOODTRUCKS_EVENTS (FOODTRUCK_ID, EVENT_ID) values (200001, 200001);