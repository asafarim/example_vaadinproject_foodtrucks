package com.verboven.springbootjpamulti;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.verboven.springbootjpamulti.controller.FoodtruckController;
import com.verboven.springbootjpamulti.entity.Event;
import com.verboven.springbootjpamulti.entity.Foodtruck;
import com.verboven.springbootjpamulti.entity.Product;
import com.verboven.springbootjpamulti.entity.ProductType;
import com.verboven.springbootjpamulti.repository.FoodtruckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FoodtruckController.class)
public class FoodtruckControllerUnitTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FoodtruckRepository foodtruckRepository;

    private ObjectMapper mapper = new ObjectMapper();

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void givenFoodtrucks_whenGetAllFoodtrucks_thenReturnJsonArray()
            throws Exception {

        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        List<Foodtruck> allFoodtrucks = Arrays.asList(truck);

        given(foodtruckRepository.findAll()).willReturn(allFoodtrucks);

        mvc.perform(get("/foodtrucks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(truck.getName()))
                .andExpect(jsonPath("$[0].products[0].name").value(product.getName()));
    }


    @Test
    public void givenFoodtruck_whenGetFoodtruckById_thenReturnFoodtruck()
            throws Exception {
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        given(foodtruckRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(truck));

        mvc.perform(get("/foodtrucks/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(truck.getName()))
                .andExpect(jsonPath("$.products[0].name").value(product.getName()));
    }

    @Test
    public void givenFoodtruck_whenGetFoodtruckEvents_thenReturnJsonArray()
            throws Exception {
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);
        Event event = new Event();
        event.setName("Testevent");
        event.setLocation("Testlocation");
        truck.addEvent(event);

        given(foodtruckRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(truck));

        mvc.perform(get("/foodtrucks/1/events")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value(event.getName()));
    }

    @Test
    public void givenFoodtruck_whenGetFoodtruckFee_thenReturnString()
            throws Exception {
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        given(foodtruckRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(truck));

        mvc.perform(get("/foodtrucks/{id}/fee", 1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("Your fee is €" + truck.getStaffnumber()*10));
    }

    @Test
    public void whenCreateFoodtruck_thenStatus200()
            throws Exception{
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        mvc.perform(post("/foodtrucks")
                .content(convertObjectToJsonBytes(truck))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void givenFoodtruck_whenUpdateFoodtruck_thenStatus200()
            throws Exception{
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        Foodtruck edittruck = new Foodtruck();
        edittruck.setName("Edittruck");
        edittruck.setStaffnumber(4);
        Product editproduct = new Product();
        editproduct.setName("Editproduct");
        editproduct.setType(ProductType.Food);
        edittruck.addProduct(product);

        given(foodtruckRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(truck));

        mvc.perform(put("/foodtrucks/{id}",1L)
                .content(convertObjectToJsonBytes(edittruck))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeleteFoodtruck_thenStatus200()
            throws Exception{
        Foodtruck truck = new Foodtruck();
        truck.setName("Testtruck");
        truck.setStaffnumber(2);
        Product product = new Product();
        product.setName("Testproduct");
        product.setType(ProductType.Drink);
        truck.addProduct(product);

        given(foodtruckRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(truck));

        mvc.perform(delete("/foodtrucks/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }




}
